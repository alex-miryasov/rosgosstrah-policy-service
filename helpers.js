const webdriver = require('selenium-webdriver');
const { By, until, Key } = webdriver;

function Sleep (ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}

async function ScrollToElement (driver, xPath, pause = 300) {
  let element = await driver.findElement(By.xpath(xPath));
  await driver.executeScript('arguments[0].scrollIntoView()', element);
  await driver.sleep(pause);
}

async function WaitWhileCheckTitle (driver, title) {
  await driver.wait(until.titleContains(title));
}

async function WaitWhileNotDisplayed (driver, xpath) {
  await driver.wait(webdriver.until.elementLocated(By.xpath(xpath))).then(async () => {
    let displayed = await driver.findElement(By.xpath(xpath)).isDisplayed();
    await console.log('DISPLAYED: ', displayed);
    await driver.sleep(500);
    if (!!displayed) {
      await driver.sleep(1000);
      await driver.wait(webdriver.until.elementLocated(By.xpath(xpath))).then(async element => {
        await driver.wait(webdriver.until.elementIsEnabled(element)).then(async () => {
          await driver.wait(webdriver.until.elementIsNotSelected(element)).then(async () => {
            await console.log('IS DISPLAYED');
          });
        });
      });
    } else {
      await driver.sleep(500);
      await WaitWhileNotDisplayed(driver, xpath);
    }
  });
}

async function ClickingInDOM (driver, xpath) {
  let okBTN = await driver.findElement(By.xpath(xpath));
  await driver.executeScript('arguments[0].click()', await okBTN);
}

async function WriteDataInDOM (driver, field, value) {
  await driver.executeScript(`arguments[0].value='${value}';`, await driver.findElement(By.xpath(field)));
  await driver.findElement(By.xpath(field)).sendKeys(Key.ENTER);
}

async function WriteData (driver, field, value) {
  if (!!value) {
    await console.log('VALUE: ', await value);
    await driver.sleep(500);
    await driver.findElement(By.xpath(field)).sendKeys(value);
    await driver.sleep(500);
  }
}

async function WriteDataAndPressEnter (driver, field, value) {
  if (!!value) {
    await console.log('VALUE: ', await value);
    await driver.sleep(500);
    await driver.findElement(By.xpath(field)).sendKeys(value);
    await driver.sleep(1000);
    await driver.findElement(By.xpath(field)).sendKeys(Key.ENTER);
  }
}

async function ClearAndWriteData (driver, field, value, fieldSpec) {
  if (!!value) {
    let fieldTXT = await driver.findElement(By.xpath(field)).getAttribute('value');
    await console.log('FIELD TEXT: ', await fieldTXT);
    await console.log('VALUE: ', await value);
    if (!fieldTXT || fieldTXT === fieldSpec) {
      await driver.sleep(250);
      await driver.wait(driver.findElement(By.xpath(field)).clear(), 5000).then(async () => {
        await console.log('FIELD IS CLEARED');
        await driver.sleep(250);
        await driver.findElement(By.xpath(field)).sendKeys(value);
        await driver.sleep(250);
      });
    }
  }
}

async function ClearAndWriteDataAfterWaiting (driver, field, value, fieldSpec) {
  if (!!value) {
    await driver.wait(webdriver.until.elementLocated(By.xpath(field))).then(async () => {
      await driver.findElement(By.xpath(field)).then(async element => {
        await driver.wait(webdriver.until.elementIsVisible(element)).then(async () => {
          let fieldTXT = await driver.findElement(By.xpath(field)).getAttribute('value');
          await console.log('FIELD TEXT: ', await fieldTXT);
          await console.log('VALUE: ', await value);
          if (!fieldTXT || fieldTXT === fieldSpec) {
            await driver.wait(driver.findElement(By.xpath(field)).clear(), 5000).then(async () => {
              await driver.sleep(500);
              await driver.findElement(By.xpath(field)).sendKeys(value);
            });
          }
        });
      });
    });
  }
}

async function ClearEqualAndWriteData (driver, field, value, fieldSpec, specEqual) {
  if (!!value) {
    let fieldTXT = await driver.findElement(By.xpath(field)).getAttribute('value');
    await console.log('FIELD TEXT: ', await fieldTXT);
    await console.log('VALUE: ', await value);
    if ((specEqual) ? fieldTXT !== value || fieldTXT !== value && fieldTXT === fieldSpec : !fieldTXT || fieldTXT === fieldSpec) {
      await driver.wait(driver.findElement(By.xpath(field)).clear(), 5000).then(async () => {
        await driver.sleep(500);
        await driver.findElement(By.xpath(field)).sendKeys(value);
      });
    }
  }
}

async function ClickingAfterWaiting (driver, wayToClick, elementIs) {
  async function Click () {
    try {
      await driver.findElement(By.xpath(wayToClick)).click();
    } catch (err) {
      await console.log('FAILED TO CLICK, XPATH: ', wayToClick);
      await console.log('FAILED TO CLICK, ERROR: ', err);
    }
  }

  await driver.wait(webdriver.until.elementLocated(By.xpath(wayToClick))).then(async element => {
    switch (elementIs) {
      case 'Enabled':
        return driver.wait(webdriver.until.elementIsEnabled(element)).then(async () => {
          await Click();
        });
      case 'Disabled':
        return driver.wait(webdriver.until.elementIsDisabled(element)).then(async () => {
          await Click();
        });
      case 'Visible':
        return driver.wait(webdriver.until.elementIsVisible(element)).then(async () => {
          await Click();
        });
      case 'NotVisible':
        return driver.wait(webdriver.until.elementIsNotVisible(element)).then(async () => {
          await Click();
        });
      case 'Selected':
        return driver.wait(webdriver.until.elementIsSelected(element)).then(async () => {
          await Click();
        });
      case 'NotSelected':
        return driver.wait(webdriver.until.elementIsNotSelected(element)).then(async () => {
          await Click();
        });
    }
  });
}

async function ClickAndWriteData (driver, field, value, pause = 500) {
  await driver.findElement(By.xpath(field)).click();
  await driver.sleep(pause);
  await driver.findElement(By.xpath(field)).sendKeys(value);
}

module.exports.Sleep = Sleep;
module.exports.ScrollToElement = ScrollToElement;
module.exports.WaitWhileCheckTitle = WaitWhileCheckTitle;
module.exports.WaitWhileNotDisplayed = WaitWhileNotDisplayed;
module.exports.ClickingInDOM = ClickingInDOM;
module.exports.WriteDataInDOM = WriteDataInDOM;
module.exports.WriteData = WriteData;
module.exports.WriteDataAndPressEnter = WriteDataAndPressEnter;
module.exports.ClearAndWriteData = ClearAndWriteData;
module.exports.ClearAndWriteDataAfterWaiting = ClearAndWriteDataAfterWaiting;
module.exports.ClearEqualAndWriteData = ClearEqualAndWriteData;
module.exports.ClickingAfterWaiting = ClickingAfterWaiting;
module.exports.ClickAndWriteData = ClickAndWriteData;
